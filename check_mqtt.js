const mqtt = require('mqtt')

// variables
let host = '127.0.0.1'
let port = 1883
let topic 
let warnings = []
let criticals = []

// loop through cli arguments
for (let i = 2; i < process.argv.length; i += 2) {
    if (process.argv[i + 1] === undefined) {
        process.stdout.write("[UNKNOWN] - No argument for " + process.argv[i])
        process.exit(3)
    }
    switch (process.argv[i]) {
        case '-h':
            host = process.argv[i + 1]
            break
        case '-p':
            port = process.argv[i + 1]
            break
        case '-t':
            topic = process.argv[i + 1].split(',')
            break
        case '-c':
            try {
                if (! /-?\d+:-?\d*/.test(process.argv[i + 1])) {
                    process.stdout.write("[UNKNOWN] - Wrong or empty criticals range was provided ")
                    process.exit(3)
                } else {
                    criticals = process.argv[i + 1].split(':')
                }
            } catch (e) {
                process.stdout.write("[UNKNOWN] - No criticals range was provided ")
                process.exit(3)
            }
            break
        case '-w':
            try {
                if (! /-?\d+:-?\d*/.test(process.argv[i + 1])) {
                    process.stdout.write("[UNKNOWN] - Wrong or empty warnings range was provided ")
                    process.exit(3)
                } else {
                    warnings = process.argv[i + 1].split(':')
                }
            } catch (e) {
                process.stdout.write("[UNKNOWN] - No warnings range was provided ")
                process.exit(3)
            }
            break
        case '-j':
            break
        default:
            process.stdout.write("[UNKNOWN] - Invalid option " + process.argv[i])
            process.exit(3)
    }

}

// if no topic were specified
if (topic === undefined) {
    process.stdout.write("[UNKNOWN] - No topic provided.\nPlease add one in the arguments ")
    process.exit(3)
}

const client = mqtt.connect(`mqtt://${host}:${port}`)

client.on('connect', () => {
    client.subscribe(topic, function (err) {
        if (!err) {
        }
    })
})


client.on('offline', () => {
    client.end()
    process.stdout.write("[CRITICAL] - MQTT server disconnected during execution")
    process.exit(2)
})

client.on('error', err => {
    client.end()
    process.stdout.write("[CRITICAL] - Error has been encountered:\n" + err)
    process.exit(2)
})

client.on('message', (t, message) => {

    let value = parseFloat(message.toString())

    if (warnings.length == 0 && criticals.length == 0) {

        process.stdout.write(`[OK] - Received on topic ${t}: ${value}|${t}=${value}`)
        process.exit(0)

    } else if (warnings.length != 0 && criticals.length != 0) {
        if (criticals[0].toString().startsWith('@') && value >= criticals[0].slice(1) && value <= criticals[1]) {

            process.stdout.write(`[CRITICAL] - Received value that exceds critical threshold on topic ${t}: ${value}|${t}=${value};${warnings[0]}:${warnings[1]};${criticals[0]}:${criticals[1]}`)
            process.exit(2)

        } else if (warnings[0].toString().startsWith('@') && value >= warnings[0].slice(1) && value <= warnings[1]) {

            process.stdout.write(`[WARNING] - Received value that exceds warning threshold on topic ${t}: ${value}|${t}=${value};${warnings[0]}:${warnings[1]};${criticals[0]}:${criticals[1]}`)
            process.exit(1)

        } else if (!criticals[0].toString().startsWith('@') && (value < criticals[0] || value > criticals[1])) {

            process.stdout.write(`[CRITICAL] - Received value that exceds critical threshold on topic ${t}: ${value}|${t}=${value};${warnings[0]}:${warnings[1]};${criticals[0]}:${criticals[1]}`)
            process.exit(2)

        } else if (!warnings[0].toString().startsWith('@') && (value < warnings[0] || value > warnings[1])) {

            process.stdout.write(`[WARNING] - Received value that exceds warning threshold on topic ${t}: ${value}|${t}=${value};${warnings[0]}:${warnings[1]};${criticals[0]}:${criticals[1]}`)
            process.exit(1)
        }
        else {

            process.stdout.write(`[OK] - Received value on topic ${t}: ${value}|${t}=${value};${warnings[0]}:${warnings[1]};${criticals[0]}:${criticals[1]}`)
            process.exit(0)

        }
    } else if (warnings.length != 0 && criticals.length == 0) {

        if (warnings[0].toString().startsWith('@') && value >= warnings[0].slice(1) && value <= warnings[1]) {
            process.stdout.write(`[WARNING] - Received value that exceds warning threshold on topic ${t}: ${value}|${t}=${value};${warnings[0]}:${warnings[1]}`)
            process.exit(1)

        } else if (!warnings[0].toString().startsWith('@') && (value < warnings[0] || value > warnings[1])) {

            process.stdout.write(`[WARNING] - Received value that exceds warning threshold on topic ${t}: ${value}|${t}=${value};${warnings[0]}:${warnings[1]}`)
            process.exit(1)

        } else {
            process.stdout.write(`[OK] - Received value on topic ${t}: ${value}|${t}=${value};${warnings[0]}:${warnings[1]}`)
            process.exit(0)
        }
    } else if (warnings.length == 0 && criticals.length != 0) {

        if (criticals[0].toString().startsWith('@') && value >= criticals[0].slice(1) && value <= criticals[1]) {

            process.stdout.write(`[CRITICAL] - Received value that exceds critical threshold on topic ${t}: ${value}|${t}=${value};;${criticals[0]}:${criticals[1]}`)
            process.exit(2)

        } else if (!criticals[0].toString().startsWith('@') && (value < criticals[0] || value > criticals[1])) {

            process.stdout.write(`[CRITICAL] - Received value that exceds critical threshold on topic ${t}: ${value}|${t}=${value};;${criticals[0]}:${criticals[1]}`)
            process.exit(2)

        } else {

            process.stdout.write(`[OK] - Received value on topic ${t}: ${value}|${t}=${value};;${criticals[0]}:${criticals[1]}`)
            process.exit(0)

        }
    }

})

