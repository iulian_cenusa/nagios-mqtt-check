const mqtt = require('mqtt')    // import MQTT library
require('dotenv').config()      // import dotenv library

// MQTT related variables
let host = '127.0.0.1'
let port = 1883
let topic = ''

// other variables
let nagios_message = ''
let performance_data = ''

// loop through cli arguments
for (let i = 2; i < process.argv.length; i += 2) {

    if (process.argv[i + 1] === undefined) {
        process.stdout.write("[UNKNOWN] - No argument for " + process.argv[i])
        process.exit(3)
    }

    switch (process.argv[i]) {
        case '-H':
            host = process.argv[i + 1]
            break
        case '-P':
            port = process.argv[i + 1]
            break
        case '-T':
            topic = process.argv[i + 1].split(',')
            break
        default:
            process.stdout.write("[UNKNOWN] - Invalid option " + process.argv[i])
            process.exit(3)
    }

}

// if no topic were specified
if (topic === undefined) {
    process.stdout.write("[UNKNOWN] - No topic provided.\nPlease add one in the arguments ")
    process.exit(3)
}


var mqtt_opt = {
    clientId: 'mqttjs_' + Math.random().toString(16).substr(2, 8),
    username: process.env.MQTT_USER,
    password: process.env.MQTT_PASSWORD,
    clean: true,
};

var client = mqtt.connect(`mqtt://${host}:${port}`, mqtt_opt);

client.on('connect', () => {
    client.subscribe(topic, err => {
        if (!err) {
        }
    })

})


client.on('offline', () => {
    client.end()
    process.stdout.write("[CRITICAL] - MQTT server disconnected during execution")
    process.exit(2)
})

client.on('error', err => {
    client.end()
    process.stdout.write("[CRITICAL] - " + err)
    process.exit(2)
})

client.on('message', (t, message) => {

    value = JSON.parse(message.toString())

    //get temperature and humidity
    if (value.hasOwnProperty('temperature')){
        nagios_message += ` Temperature: ${value.temperature}*C`
        performance_data += ` temperature=${value.temperature}degC;23;28;10;35`
    }

    if (value.hasOwnProperty('humidity')){
        nagios_message += ` Humidity: ${value.humidity}%`
        performance_data += ` humidity=${value.humidity}%;80;90;;`
    }

    // check for bmp280 sensor
    if (value.hasOwnProperty('bmp')){
        nagios_message += ` BMP280 Temperture: ${value.bmp.temperature}*C`
        performance_data += ` bmp280_temperture=${value.bmp.temperature}degC;23;28;-20;35`
        nagios_message += ` BMP280 Athmosferic Pressue: ${value.bmp.pressure} Pa`
        performance_data += ` bmp280_pressure=${value.bmp.pressure}Pa;;;;`
    }

    //check for both temperature and humidity
    if (value.hasOwnProperty('temperature') && value.hasOwnProperty('humidity')) {
        // calculate dew point
        let H = (((Math.log10(value['humidity']) - 2) / 0.4343) + ((17.62 * value['temperature']) / (243.12 + value['temperature']))).toFixed(2)
        let dew_point = ((243.12 * H) / (17.62 - H)).toFixed(2)
        nagios_message += ` Dew Point: ${dew_point}`
        performance_data += ` dew_point=${dew_point};;;-10;50`

        // calculate thermal index
        // DIA = 0,81 T + 0,01U (0,99T + 14,3) + 46,3 
        // Ranging from difficult to bear the cold (<55) to difficult to bear the heat (>80)
        let dia = 0.81 * value['temperature'] + 0.01* value['humidity'] * ( 0.99 * value['temperature'] + 14.3) + 46.3
        dia = dia.toFixed(2)
        nagios_message += ` Discomfort Index: ${dia}`
        performance_data += ` discomfort_index=${dia};70;80;30;100`

        //ITU = (T + 32 x 1.8) - (0.55 -0.0055 x U) [(T x 1.8 + 32) - 58] (from comfort– alert - to discomfort)
        // <= 65 - OK / 66-79 WARNING / >=80 CRITICAL
        let thi = (value['temperature'] + 32*1.8) - (0.55 -0.0055*value['humidity'])*((value['temperature']*1.8 + 32) - 58)
        thi = thi.toFixed(2)
        nagios_message += ` Temperature-Humidity Index: ${thi}`
        performance_data += ` temperature_humidity_index=${thi};66;80;30;100`

    }

    // check for rssi signal if it was sent 
    if (value.hasOwnProperty('rssi')) {
        nagios_message += ` RSSI: ${value['rssi']}dBm`
        //performance_data += ` rssi=${value['rssi']};;;;`
    }

    process.stdout.write(`[OK] - Received on topic ${t}:${nagios_message}|${performance_data}`)
    process.exit(0)

})
