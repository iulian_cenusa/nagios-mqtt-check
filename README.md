# Nagios Core and Nagios XI MQTT JS check

This is an extension for Nagios Core and Nagios XI. It provides the possibility to check MQTT services. It was build using Javascript.

# Usage and prerequisites

In order to use this extension you have to install NodeJS.

This script can be used standalone as command line tool or integrated with nagios.

## Command line tool

This script supports the following arguments:

### -H HOSTNAME
### -T TOPIC
### -P PORT

HOSTNAME is the name or IP of the MQTT broker used. Default is 127.0.0.1.

TOPIC is the MQTT topic on which the script will listen.

PORT is the MQTT broker port used to connect. Default is 1883.

Example: node check_mqtt_json -T test

This will listen on mqtt://localhost:1883 on topic test.

# Nagios integration

## Nagios XI

For Nagios XI it is very simple. Just go to Admin tab -> System Extensions -> Manage Plugins and there browse for this script and after that click Upload Plugin. 

![Nagios XI plugins](./images/xi_1.png)

After that you can use it to configure commands and service checks. You can see bellow and example.

![Nagios XI commands](./images/xi_2.png)

## Nagios Core 

For nagios core you have to put the script in your nagios plugins location folder.

Usually is /usr/local/nagios/libexec/

After that you have to edit configuration files in order to implement the command and service check.

These files are located usually in /usr/local/nagios/etc/objects/

Please see bellow the example for command and service check implementation.

![Nagios Core commands](./images/core_1.png)

![Nagios Core services](./images/core_2.png)
